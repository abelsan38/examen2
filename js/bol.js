function calcularPrecio() {
    var tipoViaje = document.getElementById("tipoViaje").value;
    var precio = document.getElementById("precio").value;
    if (tipoViaje === "2") {
        precio = precio * 1.8; // Incrementar precio en un 80% para viaje doble
    }
    document.getElementById("precio").value = precio;
}
function calcularVenta() {
    var precio = document.getElementById("precio").value;
    var numBoletos = document.getElementById("numBoleto").value;
    var subtotal = precio * 1;
    var impuestos = subtotal * 0.16;
    var total = subtotal + impuestos;
    document.getElementById("subtotal").innerHTML = "Subtotal: $" + subtotal.toFixed(2);
    document.getElementById("impuestos").innerHTML = "Impuestos (IVA 16%): $" + impuestos.toFixed(2);
    document.getElementById("total").innerHTML = "Total: $" + total.toFixed(2);
}
function limpiar() {
    document.getElementById("subtotal").innerHTML = "";
    document.getElementById("impuestos").innerHTML = "";
    document.getElementById("total").innerHTML = "";
    var tipoViaje = document.getElementById("tipoViaje").value = "";
    var precio = document.getElementById("precio").value = "";
    document.getElementById('numBoleto').value = "";
    document.getElementById('nombreCliente').value = "";
    document.getElementById('destino').value = "";

}
