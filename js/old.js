function generarEdades() {
    const edades = [];
    const bebes = [];
    const ninos = [];
    const adolescentes = [];
    const adultos = [];
    const ancianos = [];

    for (let i = 0; i < 100; i++) {
        const edad = Math.floor(Math.random() * 91); // Genera edades aleatorias de 0 a 90 años.
        edades.push(edad);

        if (edad < 1) {
            bebes.push(edad);
        } else if (edad >= 1 && edad < 13) {
            ninos.push(edad);
        } else if (edad >= 13 && edad < 18) {
            adolescentes.push(edad);
        } else if (edad >= 18 && edad < 65) {
            adultos.push(edad);
        } else {
            ancianos.push(edad);
        }
    }

    document.getElementById("edades").textContent = "Edades: " + edades.join(", ");
    document.getElementById("bebesCount").textContent = bebes.length;
    document.getElementById("ninosCount").textContent = ninos.length;
    document.getElementById("adolescentesCount").textContent = adolescentes.length;
    document.getElementById("adultosCount").textContent = adultos.length;
    document.getElementById("ancianosCount").textContent = ancianos.length;
}
function limpiar() {
    document.getElementById("edades").textContent = "";
    document.getElementById("bebesCount").textContent = "";
    document.getElementById("ninosCount").textContent = "";
    document.getElementById("adolescentesCount").textContent = "";
    document.getElementById("adultosCount").textContent = ""
    document.getElementById("ancianosCount").textContent = "";
}