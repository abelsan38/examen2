function convertirGrados() {
    const valorInput = parseFloat(document.getElementById("valor").value);
    const aFahrenheit = document.getElementById("aFahrenheit").checked;
    const resultadoElement = document.getElementById("resultado");

    if (aFahrenheit) {
        const fahrenheit = (valorInput * 9 / 5) + 32;
        resultadoElement.textContent = `${valorInput} grados Celsius son ${fahrenheit.toFixed(2)} grados Fahrenheit.`;
    } else {
        const celsius = (valorInput - 32) * 5 / 9;
        resultadoElement.textContent = `${valorInput} grados Fahrenheit son ${celsius.toFixed(2)} grados Celsius.`;
    }
}
function limpiar() {
    const resultadoElement = document.getElementById("resultado");
    resultadoElement.textContent = "";
}